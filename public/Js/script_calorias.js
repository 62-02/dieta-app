let datosCalorias = {
    'Lunes': 0,
    'Martes': 0,
    'Miércoles': 0,
    'Jueves': 0,
    'Viernes': 0,
    'Sábado': 0,
    'Domingo': 0,
};

let myChart; // Referencia global para la gráfica

function agregarCalorias() {
    const dia = document.getElementById('dia').value;
    const calorias = parseInt(document.getElementById('calorias').value, 10);
    datosCalorias[dia] = calorias;
    actualizarGrafica();
}

function actualizarGrafica() {
    const ctx = document.getElementById('graficaCalorias').getContext('2d');

    if (myChart) {
        myChart.destroy();
    }

    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: Object.keys(datosCalorias),
            datasets: [{
                label: 'Calorías Consumidas',
                data: Object.values(datosCalorias),
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}

// Inicializar la gráfica al cargar la página
document.addEventListener('DOMContentLoaded', actualizarGrafica);
